<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day9.txt'));

$entries = array_map('intval', $lines);

$previousAmount = 25;

// Part 1
$invalidIndex;
foreach (range($previousAmount, sizeof($entries) - 1) as $sumIndex) {
    $sumFound = false;
    foreach (range($sumIndex - $previousAmount, $sumIndex - 2) as $summand1Index) {
        foreach (range($summand1Index + 1, $sumIndex - 1) as $summand2Index) {
            if ($entries[$summand1Index] + $entries[$summand2Index] == $entries[$sumIndex]) {
                $sumFound = true;
                break 2;
            }
        }
    }
    if (!$sumFound) {
        $invalidIndex = $sumIndex;
        break;
    }
}
echo $entries[$invalidIndex] . PHP_EOL;

// Part 2
$rangeStartIndex = 0;
$rangeEndIndex = 1;
$rangeSum = $entries[$rangeStartIndex] + $entries[$rangeEndIndex];
while ($rangeSum !== $entries[$invalidIndex]) {
    $rangeSum += $rangeSum < $entries[$invalidIndex] ? $entries[++$rangeEndIndex] : -$entries[$rangeStartIndex++];
}
$range = array_slice($entries, $rangeStartIndex, $rangeEndIndex - $rangeStartIndex);
sort($range);
echo reset($range) + end($range) . PHP_EOL;