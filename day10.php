<?php

$lines = explode(PHP_EOL, file_get_contents('../inputs/day10.txt'));

$jolts = array_map('intval', $lines);
sort($jolts);
array_unshift($jolts, 0);
array_push($jolts, end($jolts) + 3);

// Part 1
$differences = array_fill(0, sizeof($jolts) - 1, -1);
foreach ($jolts as $i => $jolt) {
    if ($i > 0) {
        $differences[$i - 1] = $jolt - $jolts[$i - 1];
    }
}
$counts = array_count_values($differences);
echo $counts[1] * $counts[3] . PHP_EOL;

// Part 2
$device = end($jolts);
$waysToJolt = [];
function calculateWays($jolt) {
    if ($jolt == $GLOBALS['device']) {
        return 1;
    }
    if (!in_array($jolt, $GLOBALS['jolts'])) {
        return 0;
    }

    global $waysToJolt;
    if (array_key_exists($jolt, $waysToJolt)) {
        return $waysToJolt[$jolt];
    }
    $ways = calculateWays($jolt + 1) + calculateWays($jolt + 2) + calculateWays($jolt + 3);
    $waysToJolt[$jolt] = $ways;
    return $ways;
}
echo calculateWays(0) . PHP_EOL;