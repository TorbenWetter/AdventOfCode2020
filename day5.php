<?php

$seatNames = explode(PHP_EOL, file_get_contents('../inputs/day5.txt'));

$seatIds = array_map(function (string $seatName) {
    list($rowName, $colName) = str_split($seatName, 7);
    $rowId = bindec(str_replace(['F', 'B'], ['0', '1'], $rowName));
    $colId = bindec(str_replace(['L', 'R'], ['0', '1'], $colName));
    return $rowId * 8 + $colId;
}, $seatNames);

// Part 1
echo max($seatIds) . PHP_EOL;

// Part 2
sort($seatIds);
$allSeatIds = range(min($seatIds), max($seatIds));
$missingSeatId = array_values(array_diff($allSeatIds, $seatIds))[0];
echo $missingSeatId . PHP_EOL;