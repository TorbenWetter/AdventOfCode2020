<?php

list($ruleLines, $myTicketLines, $nearbyTicketLines) = explode(PHP_EOL . PHP_EOL, file_get_contents('../inputs/day16.txt'));

$rules = [];
foreach (explode(PHP_EOL, $ruleLines) as $ruleLine) {
    preg_match('/^(?<field>[a-z\s]+): (?<min1>[0-9]+)\-(?<max1>[0-9]+) or (?<min2>[0-9]+)\-(?<max2>[0-9]+)$/', $ruleLine, $matches);
    extract($matches);
    $rules[$field] = [
        [intval($min1), intval($max1)],
        [intval($min2), intval($max2)]
    ];
}
$myTicketNumbers = array_map(
    'intval',
    explode(',', explode(PHP_EOL, $myTicketLines)[1])
);
$nearbyTickets = array_map(
    fn ($nearbyTicketLine) => array_map(
        'intval',
        explode(',', $nearbyTicketLine)
    ),
    array_slice(explode(PHP_EOL, $nearbyTicketLines), 1)
);

// Part 1
$validValues = [];
foreach ($rules as [[$min1, $max1], [$min2, $max2]]) {
    array_push($validValues, ...range($min1, $max1), ...range($min2, $max2));
}
$validValues = array_unique($validValues);

$errorRate = 0;
foreach ($nearbyTickets as $i => $nearbyTicket) {
    $invalidSum = array_sum(array_diff($nearbyTicket, $validValues));
    if ($invalidSum > 0) {
        $errorRate += $invalidSum;
        unset($nearbyTickets[$i]);
    }
}
echo $errorRate . PHP_EOL;

// Part 2
$tickets = array_merge([$myTicketNumbers], $nearbyTickets);
$possibleIndices = [];
foreach ($rules as $field => [[$min1, $max1], [$min2, $max2]]) {
    $allValidIndices = array_map(
        fn ($ticket) => array_keys(
            array_filter(
                $ticket,
                fn ($value, $i) => $min1 <= $value && $value <= $max1 || $min2 <= $value && $value <= $max2,
                ARRAY_FILTER_USE_BOTH
            )
        ),
        $tickets
    );
    array_push($possibleIndices, [
        'field' => $field,
        'indices' => array_intersect(...$allValidIndices)
    ]);
}

usort($possibleIndices, fn ($obj1, $obj2) => sizeof($obj1['indices']) - sizeof($obj2['indices']));
$foundIndices = [];
$departureProduct = 1;
foreach ($possibleIndices as $i => $obj) {
    extract($obj);
    $fieldIndex = current(array_diff($indices, $foundIndices));
    array_push($foundIndices, $fieldIndex);
    if (substr($field, 0, 9) === 'departure') {
        $departureProduct *= $myTicketNumbers[$fieldIndex];
    }
}
echo $departureProduct . PHP_EOL;