<?php

$groupBlocks = explode(PHP_EOL . PHP_EOL, file_get_contents('../inputs/day6.txt'));

// Part 1
$count = 0;
foreach ($groupBlocks as $groupBlock) {
    $groupAnswersCombined = str_split(str_replace(PHP_EOL, '', $groupBlock)); // array(string)
    $count += sizeof(array_unique($groupAnswersCombined));
}
echo $count . PHP_EOL;

// Part 2
$count = 0;
foreach ($groupBlocks as $groupBlock) {
    $groupAnswers = array_map(fn ($answer) => str_split($answer), explode(PHP_EOL, $groupBlock)); // array(array(string))
    $count += sizeof(array_intersect(...$groupAnswers));
}
echo $count . PHP_EOL;