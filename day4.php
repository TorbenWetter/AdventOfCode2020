<?php

$passportBlocks = explode(PHP_EOL . PHP_EOL, file_get_contents('../inputs/day4.txt'));

$passports = array_map(function (string $passportBlock) {
    $passportData = explode(' ', str_replace(PHP_EOL, ' ', $passportBlock));
    $passport = [];
    foreach ($passportData as $fieldData) {
        list($fieldKey, $fieldValue) = explode(':', $fieldData);
        $passport[$fieldKey] = $fieldValue;
    }
    return $passport;
}, $passportBlocks);

// Part 1
$validPassports = array_filter($passports, function ($passport) {
    $fieldAmount = sizeof($passport);
    return $fieldAmount === 8 || ($fieldAmount === 7 && !array_key_exists('cid', $passport));
});
echo sizeof($validPassports) . PHP_EOL;

// Part 2
$validPassports = array_filter($passports, function ($passport) {
    $fieldAmount = sizeof($passport);
    if ($fieldAmount < 7 || ($fieldAmount === 7 && array_key_exists('cid', $passport))) {
        return false;
    }

    if (!preg_match("/^(19[2-9]\d|200[0-2])$/", $passport['byr'])) {
        return false;
    }
    if (!preg_match("/^(201\d|2020)$/", $passport['iyr'])) {
        return false;
    }
    if (!preg_match("/^(202\d|2030)$/", $passport['eyr'])) {
        return false;
    }
    if (!preg_match("/^((1[5-8]\d|19[0-3])cm|(59|6\d|7[0-6])in)$/", $passport['hgt'])) {
        return false;
    }
    if (!preg_match("/^#[a-z0-9]{6}$/", $passport['hcl'])) {
        return false;
    }
    if (!preg_match("/^(amb|blu|brn|gry|grn|hzl|oth)$/", $passport['ecl'])) {
        return false;
    }
    if (!preg_match("/^[0-9]{9}$/", $passport['pid'])) {
        return false;
    }

    return true;
});
echo sizeof($validPassports) . PHP_EOL;